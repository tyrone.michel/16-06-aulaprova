#!/bin/bash

checa_yad() {
    if ! command -v "$1" &> /dev/null; then
        return 1
    fi
}

instala_yad() {
    if command -v apt-get &> /dev/null; then
        sudo apt-get install -y "$1"
    elif command -v yum &> /dev/null; then
        sudo yum install -y "$1"
    elif command -v dnf &> /dev/null; then
        sudo dnf install -y "$1"
    else
        echo "Erro: Sem gerenciador."
        exit 1
    fi
}

checa_yad2() {
    checa_yad "yad"
    return $?
}

instala_yad2() {
    echo "Sem pacote YAD instalado."
    echo "Baixar o YAD?"
    select yn in "Sim" "Não"; do
        case $yn in
            Sim )
                instala_yad "yad"
                break
                ;;
            Não )
                echo "YAD necessário para o script. Instale e execute novamente."
                exit 1
                ;;
        esac
    done
}

randsenha() {
    local tamanho=$1
    local opcoes=$2

    local senhas=""
    for i in {1..3}; do
        senha=$(LC_ALL=C tr -dc "$opcoes" </dev/urandom | head -c "$tamanho")
        senhas+="Senha $i: $senha"$'\n'
    done

    echo "$senhas"
}

main() {
    if ! checa_yad2; then
        instala_yad2
    fi

    local tamanho
    local incluir_maiusculas
    local incluir_minusculas
    local incluir_numeros
    local incluir_especiais

    while true; do
        yad --title "Gerador de 3 Senhas Aleatórias" \
            --text="Instrução: Escolha uma ou mais opções, caso nenhuma seja escolhida o programa marcará todas as opções para você" \
            --form \
            --field="Escolha o tamanho da senha, ela deve ser maior que zero.:NUM" \
            --field="Letras Maiúsculas:CHK" \
            --field="Letras Minúsculas:CHK" \
            --field="Números:CHK" \
            --field="Caracteres Especiais:CHK" \
            --button="Gerar Agora!gtk-ok:0" \
            --button="Fechar!gtk-close:1" \
            --center \
            --width=400 \
            --height=300 > tmp.txt

        local retorno=$?
        if [ $retorno -eq 0 ]; then
            tamanho=$(awk -F '|' '{print $1}' tmp.txt)
            incluir_maiusculas=$(awk -F '|' '{print $2}' tmp.txt)
            incluir_minusculas=$(awk -F '|' '{print $3}' tmp.txt)
            incluir_numeros=$(awk -F '|' '{print $4}' tmp.txt)
            incluir_especiais=$(awk -F '|' '{print $5}' tmp.txt)

            if [ "$incluir_maiusculas" = "FALSE" ] && [ "$incluir_minusculas" = "FALSE" ] && [ "$incluir_numeros" = "FALSE" ] && [ "$incluir_especiais" = "FALSE" ]; then
                incluir_maiusculas="TRUE"
                incluir_minusculas="TRUE"
                incluir_numeros="TRUE"
                incluir_especiais="TRUE"
            fi

            local opcoes=""

            if [ "$incluir_maiusculas" = "TRUE" ]; then
                opcoes+="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            fi

            if [ "$incluir_minusculas" = "TRUE" ]; then
                opcoes+="abcdefghijklmnopqrstuvwxyz"
            fi

            if [ "$incluir_numeros" = "TRUE" ]; then
                opcoes+="0123456789"
            fi

            if [ "$incluir_especiais" = "TRUE" ]; then
                opcoes+="!@#$%^&*()"
            fi

            if [ $tamanho -gt 0 ]; then
                if [ -n "$opcoes" ]; then
                    local senhas=$(randsenha "$tamanho" "$opcoes")
                    echo -e "$senhas" | yad --title "Senhas Geradas" \
                        --text-info \
                        --button="Gerar Novamente!gtk-ok:0" \
                        --button="Fechar!gtk-close:1" \
                        --center \
                        --width=400 \
                        --height=300

                    retorno=$?
                    if [ $retorno -ne 0 ]; then
                        break
                    fi
                fi
            else
                yad --title "Erro!" \
                    --text="O tamanho da senha deve ser maior que 0." \
                    --button="OK!gtk-ok:0" \
                    --center \
                    --width=400 \
                    --height=150
            fi
        else
            break
        fi
    done

    rm -f tmp.txt
}

main

