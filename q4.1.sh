#!/bin/bash

atacar() {
    local ataque_player=$((RANDOM % 91 + 10))
    dragaozao_vida=$((dragaozao_vida - ataque_player))
    echo "$player_nome ataca o $dragaozao_nome com $ataque_player pontos de vida."
    if [ $dragaozao_vida -le 0 ]; then
        echo "Parabéns! Você derrotou o $dragaozao_nome!"
        return 1
    fi

    local ataques_drago=$((RANDOM % 5 + 1))
    for ((i=1; i<=$ataques_drago; i++)); do
        local ataque_drago=$((RANDOM % 10 + 1))
        player_vida=$((player_vida - ataque_drago))
        echo "$dragaozao_nome ataca o $player_nome com $ataque_drago pontos de vida."
        if [ $player_vida -le 0 ]; then
            echo "O $dragaozao_nome te comeu!"
            return 2
        fi
    done
}

fugir() {
    local chance=$((RANDOM % 5))
    if [ $chance -eq 0 ]; then
        echo "Você conseguiu fugir!"
        return 0
    else
        echo "Tomou Baforada do $dragaozao_nome! Game Over!"
        return 3
    fi
}

info() {
    echo "$player_nome: $player_vida pontos de vida"
    echo "$dragaozao_nome: $dragaozao_vida pontos de vida"
}

curar() {
    local cura=$((RANDOM % 51 + 50))
    player_vida=$((player_vida + cura))
    echo "$player_nome se cura e recupera $cura pontos de vida."
    local chance_fuga=$((RANDOM % 10))
    if [ $chance_fuga -eq 0 ]; then
        echo "$dragaozao_nome tenta fugir, mas não consegue!"
    fi
}
